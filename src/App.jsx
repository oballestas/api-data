import { useState } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { ListadoUsuarios } from "./components/ListadoUsuarios";
import { ListadoFotos } from "./components/ListadoFotos";

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className="App container">
      <h1 className="py-3">Hello CAR IV</h1>
      <ListadoUsuarios />
      <ListadoFotos />
    </div>
  );
}

export default App;
