export function Usuario({ name, username, email }) {
  return (
    <div className="col-3 mx-4 my-2">
      <div className="card" style={{ width: "14rem" }}>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">{name}</li>
          <li className="list-group-item">{username}</li>
          <li className="list-group-item">{email}</li>
        </ul>
      </div>
    </div>
  );
}
