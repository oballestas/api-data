export function Foto({ titulo, urlImagen }) {
  return (
    <div className="col-4 py-3">
      <div className="card" style={{ width: "14rem" }}>
        <img src={urlImagen} className="card-img-top" alt={titulo} />
        <div className="card-body">
          <p className="card-text">{titulo}</p>
        </div>
      </div>
    </div>
  );
}
