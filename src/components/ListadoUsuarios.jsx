import { Section } from "./Section";
import { Usuario } from "./Usuario";
import { useEffect, useState } from "react";

export function ListadoUsuarios() {
  let [usuario, setUsuario] = useState(null);
  const ULR_USERS = "https://jsonplaceholder.typicode.com/users";
  useEffect(() => {
    fetch(ULR_USERS)
      .then((response) => response.json())
      .then((datos) => setUsuario(datos));
  }, []);

  return (
    <Section titulo="Usuarios">
      <div className="container">
        <div className="row col-12">
          {usuario ? (
            usuario.map((user) => {
              return (
                <Usuario
                  key={user.username}
                  name={user.name}
                  username={user.username}
                  email={user.email}
                />
              );
            })
          ) : (
            <h3>Cargando usuarios...</h3>
          )}
        </div>
      </div>
    </Section>
  );
}
