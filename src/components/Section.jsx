export function Section({ titulo, children }) {
  return (
    <section>
      <h4 className="p-3">{titulo}</h4>
      {children}
    </section>
  );
}
