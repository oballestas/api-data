import { Section } from "./Section";
import { useEffect, useState } from "react";
import { Foto } from "./Foto";

export function ListadoFotos() {
  const URL_PHOTOS = "https://jsonplaceholder.typicode.com/photos";
  let [foto, setFoto] = useState(null);

  useEffect(() => {
    fetch(URL_PHOTOS)
      .then((response) => response.json())
      .then((datos) => setFoto(datos.slice(0, 21)));
  }, []);

  return (
    <Section titulo="Fotos">
      <div className="container">
        <div className="row col-12">
          {foto ? (
            foto.map((photo) => {
              return (
                <Foto
                  key={photo.id}
                  titulo={photo.title}
                  urlImagen={photo.url}
                />
              );
            })
          ) : (
            <h3>Cargando fotos...</h3>
          )}
        </div>
      </div>
    </Section>
  );
}
